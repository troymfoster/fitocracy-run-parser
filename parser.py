#!/usr/bin/python

import csv
with open('dataSimple.txt', 'rb') as csvfile:
     fitocracyReader = csv.reader(csvfile, delimiter=',', quotechar='|')
     hour = 0.0
     distanceinMeters = 0.0
     distanceinMiles = 0.0
     pushupCount = 0
     situpCount = 0
     for row in fitocracyReader:
        if (row[0] == '"Activity"'):
            #print ' : '.join(row)
            #print fitocracyReader.line_num
            # iterate through and find the field labeled Combined
            combinedLoc = row.index('"Combined"')
            hourLoc = row.index('"unit"') - 1
            #print combinedLoc
            #print hourLoc
        elif(row[0] == '"Running"'):
            #print row[hourLoc]
            if(row[hourLoc] != ''):
                #print row[hourLoc]
                hour +=  float(row[hourLoc])
            #print row[combinedLoc].strip('"')
            combinedReader = row[combinedLoc].strip('"').split(' ')
            #for word in combinedReader:
            #    print word
            if(combinedReader.count('mi') == 1):
                distanceinMiles += float(combinedReader[combinedReader.index('mi') - 1])

            elif(combinedReader.count('m') == 1):
                distanceinMeters += float(combinedReader[combinedReader.index('m') - 1])
            elif(combinedReader.count('km') == 1):
                distanceinMeters += float(combinedReader[combinedReader.index('km') - 1]) * 1000
            #else:
            #print row[combinedLoc].strip('"')
        elif(row[0] == '"Push-Up"'):
            pushupCount += int(row[hourLoc])
        elif(row[0] == '"Sit-Up"'):
            situpCount += int(row[hourLoc])


         #print fitocracyReader.line_num
     print 'total hours = '
     print hour
     print 'total miles = '
     print distanceinMiles
     print 'total meters = '
     print distanceinMeters
     print 'total distance in miles = '
     print distanceinMiles + distanceinMeters / 1600
     print 'total pushups'
     print pushupCount
     print 'total situps'
     print situpCount